package kubernetes

import (
	"context"
	"fmt"

	log "github.com/sirupsen/logrus"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"
	metrics "k8s.io/metrics/pkg/client/clientset/versioned"
)


func SetupK8s(kubeconfig *string) *rest.Config {
	// use the current context in kubeconfig
	config, err := clientcmd.BuildConfigFromFlags("", *kubeconfig)
	if err != nil {
		panic(err.Error())
	}
	return config
}

func AllPodsMetrics(config *rest.Config) {
	mc, err := metrics.NewForConfig(config)
    if err != nil {
        panic(err)
    }
	podMetrics, err := mc.MetricsV1beta1().PodMetricses(metav1.NamespaceAll).List(context.TODO(), metav1.ListOptions{})

	for i := 1; i < len(podMetrics.Items); i++ {
		log.Info(fmt.Sprintf("Current resource usage for %s:", podMetrics.Items[i].Name))
		podContainers := podMetrics.Items[i].Containers
		for _, container := range podContainers {
			log.Info(fmt.Sprintf("CPU: %s", container.Usage.Cpu()))
			log.Info(fmt.Sprintf("Memory: %s", container.Usage.Memory()))
		}
	}
}

func SinglePodMetrics(config *rest.Config, pod string, namespace string) {
	mc, err := metrics.NewForConfig(config)
    if err != nil {
        panic(err)
    }
	podMetrics, err := mc.MetricsV1beta1().PodMetricses(namespace).Get(context.TODO(), pod, metav1.GetOptions{})
	if err != nil {
		log.Error(err)
	}
	container := podMetrics.Containers[0]
	log.Info(fmt.Sprintf("Current resource usage for %s:", pod))
	log.Info(fmt.Sprintf("CPU: %s", container.Usage.Cpu()))
	log.Info(fmt.Sprintf("Memory: %s", container.Usage.Memory()))
}
