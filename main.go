package main

import (
	"flag"
	"fmt"
	"os"
	"sync"
	"time"

	log "github.com/sirupsen/logrus"

	"github.com/tobifroe/krref/kubernetes"
	load "github.com/tobifroe/krref/load-testing"
	"github.com/tobifroe/krref/version"
)

func main() {
	versionFlag := flag.Bool("version", false, "Version")
	kubeconfig := flag.String("kubeconfig", "", "absolute path to the kubeconfig file")
	requests := flag.Int("requests", 1, "number of requests to send")
	perSecond := flag.Int("persecond", 2, "number of requests to send per second")
	loadTarget := flag.String("target", "http://localhost:8080", "target to send requests to")
	pod := flag.String("pod", "", "specific pod to read values from")
	namespace := flag.String("namespace", "default", "specify namespace")
	silent := flag.Bool("s", false, "hide get request output")
	flag.Parse()

	if *versionFlag {
        fmt.Println(version.Version)
		return
	}

	envKubeconfig := os.Getenv("KUBECONFIG")

	if envKubeconfig != "" {
		*kubeconfig = envKubeconfig
	}

	var wg sync.WaitGroup
	wg.Add(*requests)

	log.Info(*kubeconfig)
	log.Info(*pod)

	if *kubeconfig != "" && *pod != "" {
		log.Info("Before load:")
		restConfig := kubernetes.SetupK8s(kubeconfig)
		kubernetes.SinglePodMetrics(restConfig, *pod, *namespace)
	}

	if *silent {
		log.Info("Generating some load. Patience please...")
	}

	for i:= 1; i <= *requests; i++ {
		go load.RunLoadTest(*loadTarget, &wg, *silent)
		time.Sleep(time.Duration(1000000000 / *perSecond))
	}

	wg.Wait()

	if *kubeconfig != "" {
		restConfig := kubernetes.SetupK8s(kubeconfig)
		if *pod != "" {
			kubernetes.SinglePodMetrics(restConfig, *pod, *namespace)
		} else {
			kubernetes.AllPodsMetrics(restConfig)
		}
	}
	return
}
