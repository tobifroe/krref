package load

import (
	"fmt"
	"net/http"
	"strconv"
	"sync"

	log "github.com/sirupsen/logrus"
)

func RunLoadTest(url string, wg *sync.WaitGroup, silent bool) int {
	defer wg.Done()
	resp, err := http.Get(url)
	if err != nil {
		log.Error("An Error occurred!")
		log.Error(err)
		return 0
	}
	status := &resp.StatusCode
	statusString := strconv.Itoa(*status)
	if !silent {
		log.Info(fmt.Sprintf("Great Success! Status Code: %s", statusString))
	}
	return *status
}