# krref

[![CircleCI](https://circleci.com/gh/tobifroe/krref/tree/main.svg?style=svg&circle-token=2a3a2924e3ce4e8fb8d4be6c63122d8fdcdb8db1)](https://circleci.com/gh/tobifroe/krref/tree/main)

Generate load and measure pod resource usage.

krref is short for **K**ubernetes **R**esource **Re**quest **F**inder.

## Getting started

Download the binary for your OS/Arch from the [releases page](https://github.com/tobifroe/krref/releases).

## Usage

```console
$ ./krref -requests=50 -persecond=10 -target=http://localhost:8080 -namespace=default
```

## Arguments
```
  -kubeconfig string
    	absolute path to the kubeconfig file
  -namespace string
    	specify namespace (default "default")
  -persecond int
    	number of requests to send per second (default 2)
  -pod string
    	specific pod to read values from
  -requests int
    	number of requests to send (default 1)
  -s	hide get request output
  -target string
    	target to send requests to (default "http://localhost:8080")
  -version
    	Version
```
